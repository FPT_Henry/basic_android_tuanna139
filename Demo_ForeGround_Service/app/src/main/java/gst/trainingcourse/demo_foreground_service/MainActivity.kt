package gst.trainingcourse.demo_foreground_service

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import gst.trainingcourse.demo_foreground_service.model.Song
import gst.trainingcourse.demo_foreground_service.service.MyService

class MainActivity : AppCompatActivity() {
    private lateinit var txtDatIntent :EditText
    private lateinit var btnStartService :Button
    private lateinit var btnStopService :Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtDatIntent = findViewById(R.id.main_txt_data_intent)
        btnStartService = findViewById(R.id.main_btn_start_service)
        btnStopService = findViewById(R.id.main_btn_stop_service)

        btnStartService.setOnClickListener {
            val song = Song("buon","vu duy khanh", R.drawable.img_music,R.raw.music )
            val intent = Intent(this, MyService::class.java)
            val bundle = Bundle()
            bundle.putSerializable("data" , song)
            intent.putExtras(bundle)
            startService(intent)
        }


        btnStopService.setOnClickListener {
                val intent = Intent(this, MyService::class.java)
              //  intent.putExtra("key_data_intent", txtDatIntent.text.toString().trim() )
                stopService(intent)
        }
    }




}



