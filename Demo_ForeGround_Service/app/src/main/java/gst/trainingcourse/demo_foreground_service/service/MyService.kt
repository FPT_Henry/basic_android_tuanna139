package gst.trainingcourse.demo_foreground_service.service

import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.os.IBinder
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import gst.trainingcourse.demo_foreground_service.MainActivity
import gst.trainingcourse.demo_foreground_service.R
import gst.trainingcourse.demo_foreground_service.broadCast.MyReceiver
import gst.trainingcourse.demo_foreground_service.model.Song
import gst.trainingcourse.demo_foreground_service.service.MyApplication.Companion.CHANNEL_ID

class MyService : Service() {
    companion object{
        const val ACTION_PAUSE = 1
        const val ACTION_RESUME = 2
        const val ACTION_CLEAR = 3
    }
    private lateinit var mediaPlayer: MediaPlayer
    var isPlaying: Boolean = false
    lateinit var mSong :Song

    override fun onCreate() {
        super.onCreate()
        Log.e("TuanNA139", "start onCreate service")
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val bundle = intent?.extras
        if(bundle != null){
            val song = bundle?.get("data") as? Song
            // có string rồi thì gửi notification mà hiển thị data lên
            if (song != null){
                mSong = song
                startMusic(song)
                sendNotification(mSong)
            }
        }

        // nhận lại dữ liệu từ receiver
        val actionMusic: Int? = intent?.getIntExtra("action_music_service",0)
        if (actionMusic != null) {
            handleAction(actionMusic)
        }

        return START_NOT_STICKY
    }

    private fun startMusic(song: Song) {
        mediaPlayer = MediaPlayer.create(applicationContext,song.resource)
        mediaPlayer.start()
        isPlaying = true
    }

    fun handleAction(Action:Int){
        when(Action){
            1 -> pauseMusic()
            2 -> resumeMusic()
            3 -> clearMusic()
        }
    }
    fun pauseMusic(){
        if (isPlaying){ // true
            mediaPlayer.pause()
            isPlaying = false
            sendNotification(mSong)
        }
    }
    fun resumeMusic(){
        if(!isPlaying){ //false
            mediaPlayer.start()
            isPlaying = true
            sendNotification(mSong)
        }
    }
    fun clearMusic(){
        stopSelf()
    }

    private fun sendNotification(song: Song) {
        // khi click vào thì muốn mở lại main activity sd penddingintent
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent =PendingIntent.getActivity(
            this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            // sử dụng ảnh thì sd bitmap
        var bitmap: Bitmap = BitmapFactory.decodeResource(resources, song.image)

        val remoteView = RemoteViews(packageName, R.layout.layout_notification)
        remoteView.setTextViewText(R.id.layout_notifi_txt_title, song.title)
        remoteView.setTextViewText(R.id.layout_notifi_txt_single, song.single)
        remoteView.setImageViewBitmap(R.id.layout_notifi_txt_image,bitmap)
        remoteView.setImageViewResource(R.id.layout_notifi_img_playorpause,R.drawable.pause)


        // bắt sự kiện khi click action trên notification
        if(isPlaying){ // nếu nó đang chạy thì pause lại
            remoteView.setOnClickPendingIntent(R.id.layout_notifi_img_playorpause, getPendingIntent(this, ACTION_PAUSE))
            remoteView.setImageViewResource(R.id.layout_notifi_img_playorpause,R.drawable.pause)
        }else{
            remoteView.setOnClickPendingIntent(R.id.layout_notifi_img_playorpause, getPendingIntent(this, ACTION_RESUME))
            remoteView.setImageViewResource(R.id.layout_notifi_img_playorpause,R.drawable.play)
        }
        // tk sự kiện cancel
        remoteView.setOnClickPendingIntent(R.id.layout_notifi_img_cancel, getPendingIntent(this, ACTION_CLEAR))


        //tạo notification content
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_android_icon)
            .setContentIntent(pendingIntent)
            .setCustomContentView(remoteView)
            .setSound(null)
            .build()


        // gửi notification liên quan forceGround Service
        startForeground(1, notification)

    }

    // viết hàm pendding intent xử lý chung cho 3 sự kiện remoteView.onclick
    fun getPendingIntent(context: Context, action: Int):PendingIntent{
        val intent = Intent(this, MyReceiver::class.java)
        intent.putExtra("action_Music", action)

        return PendingIntent.getBroadcast(context.applicationContext, action, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("TuanNA139", "start onDestroy service")
            mediaPlayer.release()
    }

}