package gst.trainingcourse.demo_foreground_service.service

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager

// muốn sd forceGround thì mình phải sử dụng 1 cái notificationChannel
class MyApplication : Application() {
    companion object {
        const val CHANNEL_ID = "channel_service_example"
    }

    override fun onCreate() {
        super.onCreate()
        createChannelNotification()
    }

    private fun createChannelNotification() {
        // version tu 8.0 đổ lên
        val channel = NotificationChannel(
            CHANNEL_ID,
            "Channel Service Example",
            NotificationManager.IMPORTANCE_DEFAULT
        )
        channel.setSound(null, null)

//        val manager: NotificationManager = getSystemService(NotificationManager::class.java)
//        manager.createNotificationChannel(channel)
        val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(channel)
    }


}