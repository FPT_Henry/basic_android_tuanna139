package gst.trainingcourse.demo_foreground_service.broadCast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import gst.trainingcourse.demo_foreground_service.service.MyService


// khi click vào action ta gửi dữ liệu qua receiver rồi từ receiver chuyển action dữ liệu lại cho service
class MyReceiver : BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        val actionMusic: Int? = p1?.getIntExtra("action_Music", 0)

        val intentService = Intent(p0, MyService::class.java)
        intentService.putExtra("action_music_service", actionMusic)

        p0?.startService(intentService)
    }
}