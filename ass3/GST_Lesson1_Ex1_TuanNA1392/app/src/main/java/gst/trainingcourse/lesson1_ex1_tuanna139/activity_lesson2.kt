package gst.trainingcourse.lesson1_ex1_tuanna139

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_lesson2.*

class activity_lesson2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lesson2)
        val intent = intent
        val list: ArrayList<String> = intent.getStringArrayListExtra("data") as ArrayList<String>
        activity_lesson2_txt.text = list.toString()

    }
}