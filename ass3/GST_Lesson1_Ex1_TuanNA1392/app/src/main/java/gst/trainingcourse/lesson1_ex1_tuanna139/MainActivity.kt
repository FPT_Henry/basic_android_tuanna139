package gst.trainingcourse.lesson1_ex1_tuanna139

import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val screenOn =ScreenOnReceiver()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activity_main_btn.setOnClickListener {
            val intent: Intent = Intent(applicationContext, activity_lesson2::class.java)
            var list: ArrayList<String> = arrayListOf("Hello!", "Hi!", "Salut!", "Hallo!", "Ciao!",
                "Ahoj!", "YAH sahs!", "Bog!", "Hej!", "Czesc!", "Ní hảo!",
                "Kon’nichiwa!", "Annyeonghaseyo!", "Shalom!", "Sah-wahd-deekah!", "Merhaba!", "Hujambo!", "Olá!")
            intent.putExtra("data", list)
            startActivity(intent)
        }

        activity_main_btn_lesson3_intent.setOnClickListener {
            clickSentIntent()
        }

        val display = IntentFilter("android.intent.action.SCREEN_ON")
        //listen event Screen On
        registerReceiver(screenOn,display)
    }

    private fun clickSentIntent() {
        val sendIntent = Intent()
        sendIntent.setAction(Intent.ACTION_SEND)
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Send Intent")
        sendIntent.setType("text/plain")
        startActivity(sendIntent)
    }

    override fun onDestroy() {
        unregisterReceiver(screenOn)
        super.onDestroy()
    }



}