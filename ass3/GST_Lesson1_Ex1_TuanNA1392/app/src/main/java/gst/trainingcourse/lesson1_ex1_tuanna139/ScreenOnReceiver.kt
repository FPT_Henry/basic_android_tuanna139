package gst.trainingcourse.lesson1_ex1_tuanna139

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class ScreenOnReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (Intent.ACTION_SCREEN_ON == intent!!.action) {
            Toast.makeText(context,"Screen On",Toast.LENGTH_LONG).show()
            val launch= Intent(context,MainActivity::class.java)
            launch.flags=Intent.FLAG_ACTIVITY_NEW_TASK
            context?.startActivity(launch)
        }
    }


    }
