package com.example.boundservice

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.boundservice.MainActivity.Companion.KEY_RESULT

class FragmentResult : Fragment(R.layout.fragment_result) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var total = ""
        var tvresult: TextView = view.findViewById(R.id.tv_result)
        val bundle = arguments
        if(bundle != null){
            total = bundle.getInt(KEY_RESULT).toString()
        }
        tvresult.text = total
    }
}