package com.example.boundservice

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import android.util.Log
import java.util.*

//iBinder for client interact with service
class MusicBoundService  : Service() {
     private lateinit var mMediaPlayer: MediaPlayer

    private var binder = MyBinder()
    inner class MyBinder : Binder(){
        fun getService(): MusicBoundService = this@MusicBoundService
    }

    override fun onCreate() {
        super.onCreate()
        Log.e("MusicBoundService", "onCreate")
    }
    override fun onBind(intent: Intent?): IBinder {
        Log.e("MusicBoundService", "onBind")
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.e("MusicBoundService", "onUnBind")
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("MusicBoundService", "onDestroy")
        mMediaPlayer.release()
    }

    fun startMusic(){
        mMediaPlayer = MediaPlayer.create(applicationContext, R.raw.file_music)
        mMediaPlayer.start()
    }


}